﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PohligHellmanInThreePassProtocol.AppCode
{
    class General
    {
        public static string NewLine = "\r\n";

        private static int columnWidth = 7;
        public static int totalColumn = 7;

        public static bool isBase64(string text)
        {
            try
            {
                Convert.FromBase64String(text);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool isNumber(string number)
        {
            try
            {
                int num = Int32.Parse(number);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static string printByteArray(byte[] byteArray)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteArray.Length; i++)
            {
                sb.Append(String.Format("[{0}]", byteArray[i]));
            }
            return sb.ToString();
        }

        public static String DrawLine()
        {
            StringBuilder temp = new StringBuilder();
            temp.Append(new string('-', totalColumn * columnWidth + (totalColumn + 1)));
            temp.Append("\r\n");
            return temp.ToString();
        }

        public static String DrawRow(string[] label)
        {
            StringBuilder temp = new StringBuilder();
            for (int i = 0; i < label.Length; i++)
            {
                int width = columnWidth;
                temp.Append("|");
                if (string.IsNullOrEmpty(label[i]))
                {
                    temp.Append(new string(' ', width));
                }
                else
                {
                    temp.Append(label[i].PadRight(width - (width - label[i].Length) / 2)
                        .PadLeft(width));
                }
            }
            temp.Append("|" + NewLine);
            return temp.ToString();
        }
    }
}
