﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PohligHellmanInThreePassProtocol.AppCode
{
    class PohligHellman
    {
        private Random rnd;
        public StringBuilder sb;

        public PohligHellman()
        {
            rnd = new Random();
            sb = new StringBuilder();
        }

        public int GetP()
        {
            //List<int> prima = SieveofEratosthenes.Generate(2, 300);
            List<int> prima = SieveofEratosthenes.Generate(1000, 1000000);
            int bil = prima[rnd.Next(0, prima.Count - 1)];
            sb.Append(String.Format("Generate prime from {0} to {1}. Total prime {2}{3}", 1000, 1000000, prima.Count, General.NewLine));
            return bil;
        }

        public int GetE(int totien_p)
        {
            int e = rnd.Next(2, totien_p - 1);
            sb.Append(String.Format("Random e (1 < e < totien p -1){0}", General.NewLine));
            sb.Append(String.Format("e = {0}, ", e));
            while (GCD(totien_p, e) != 1)
            {
                e = rnd.Next(2, totien_p - 1);
                sb.Append(String.Format("e = {0}, ", e));
            }
            return e;
        }

        public int ExtendedEuclidean(int totien_p, int e)
        {
            sb.Append(String.Format("r1 = {0}, r2 = {1}{2}", totien_p, e, General.NewLine));
            int r1 = totien_p;
            int r2 = e;
            int r = 0;

            int t1 = 0;
            int t2 = 1;
            int t = 0;
            sb.Append(General.DrawLine());
            sb.Append(General.DrawRow(new string[7]{"q","r1","r2","r","t1","t2","t"}));
            sb.Append(General.DrawLine());

            while (r1 > 1)
            {
                int q = (int)r1 / r2;
                r = r1 - q * r2;
                t = t1 - q * t2;
                sb.Append(General.DrawRow(new string[7] { q.ToString(), r1.ToString(), r2.ToString(), r.ToString(), t1.ToString(), t2.ToString(), t.ToString() }));
                sb.Append(General.DrawLine());

                r1 = r2;
                r2 = r;

                t1 = t2;
                t2 = t;
            }

            sb.Append(General.DrawRow(new string[7] { "", r1.ToString(), r2.ToString(), "", t1.ToString(), t2.ToString(), "" }));
            sb.Append(General.DrawLine());

            t1 = ((t1 >= 0) ? t1 : t1 + totien_p);
            sb.Append(String.Format("{0} d = {1}{2}", General.NewLine, t1, General.NewLine));
            return t1;
        }

        public long GCD(long a, long b)
        {
            long r1 = a;
            long r2 = b;
            long r = 0;
            while (r2 > 0)
            {
                long q = (long)r1 / r2;
                r = r1 - q * r2;
                r1 = r2;
                r2 = r;
            }

            sb.Append(String.Format("GCD({0},{1}) = {2}{3}", a, b, r1, General.NewLine));
            return r1;
        }

        public long mod(long x, long m)
        {
            long r = x % m;
            return r < 0 ? r + m : r;
        }

        public long ModularExponential(long b, long e, long m)
        {
            long temp = 1;
            for (long i = 0; i < e; i++)
            {
                temp = mod((temp * b), m);
            }
            return temp;
        }

        public long Encrypt(long m, long e, long p)
        {
            return ModularExponential(m, e, p);
        }

        public string Encrypt(string message, long e, long p, bool base64 = false)
        {
            string cipherText = "";
            sb.Append("Is base64 text = " + base64 + General.NewLine);
            if (base64)
            {
                byte[] plainByte = Convert.FromBase64String(message);
                byte[] cipherByte = new byte[plainByte.Length];
                sb.Append(String.Format("{0} base64 decode = {1}{2}", message, General.printByteArray(plainByte), General.NewLine));

                for (int i = 0; i < plainByte.Length / 4; i++)
                {
                    byte[] temp = new byte[4];
                    for (int j = 0; j < 4; j++)
                    {
                        temp[j] = plainByte[i * 4 + j];
                    }
                    int plainInt = BitConverter.ToInt32(temp, 0);
                    sb.Append(String.Format("byte array {0} to int = {1}{2}", General.printByteArray(temp), plainInt, General.NewLine));
                    int encInt = (int)Encrypt(plainInt, e, p);
                    sb.Append(String.Format("Encrypt({0}) = {1} ^ {2} mod {3} = {4}{5}", plainInt, plainInt, e, p, encInt, General.NewLine));
                    byte[] encByte = BitConverter.GetBytes(encInt);
                    sb.Append(String.Format("{0} to byte array = {1}{2}", encInt, General.printByteArray(encByte), General.NewLine));
                    for (int j = 0; j < encByte.Length; j++)
                    {
                        cipherByte[i * 4 + j] = encByte[j];
                    }
                    sb.Append(General.NewLine);
                }
                cipherText = Convert.ToBase64String(cipherByte);
                sb.Append("All byte array convert to base64" + General.NewLine);
                sb.Append(String.Format("{0} = {1}{2}", General.printByteArray(cipherByte), cipherText, General.NewLine));
            }
            else {
                byte[] cipherByte = new byte[message.Length * 4];
                for (int i = 0; i < message.Length; i++)
                {
                    int plainInt = (int)message[i];
                    sb.Append(String.Format("{0} = {1} {2}", message[i], plainInt, General.NewLine));
                    int encInt = (int)Encrypt(plainInt, e, p);
                    sb.Append(String.Format("Encrypt({0}) = {1} ^ {2} mod {3} = {4}{5}", plainInt, plainInt, e, p, encInt, General.NewLine));
                    byte[] encByte = BitConverter.GetBytes(encInt);
                    sb.Append(String.Format("{0} to byte array = {1}{2}", encInt, General.printByteArray(encByte), General.NewLine));
                    for (int j = 0; j < encByte.Length; j++)
                    {
                        cipherByte[i * 4 + j] = encByte[j];
                    }
                    sb.Append(General.NewLine);
                }
                cipherText = Convert.ToBase64String(cipherByte);
                sb.Append("All byte array convert to base64" + General.NewLine);
                sb.Append(String.Format("{0} = {1}{2}", General.printByteArray(cipherByte), cipherText, General.NewLine));
            }

            return cipherText;
        }

        public long Decrypt(long c, long d, long p)
        {
            return ModularExponential(c, d, p);
        }

        public string Decrypt(string cipher, long d, long p, bool base64 = false)
        {
            string plainText = "";
            sb.Append("Is base64 text = " + base64 + General.NewLine);
            byte[] cipherByte = Convert.FromBase64String(cipher);
            byte[] plainByte = new byte[cipherByte.Length];
            sb.Append(String.Format("{0} base64 decode = {1}{2}", cipher, General.printByteArray(cipherByte), General.NewLine));

            for (int i = 0; i < cipherByte.Length / 4; i++)
            {
                byte[] temp = new byte[4];
                for (int j = 0; j < 4; j++)
                {
                    temp[j] = cipherByte[i * 4 + j];
                }
                int cipherInt = BitConverter.ToInt32(temp, 0);
                sb.Append(String.Format("byte array {0} to int = {1}{2}", General.printByteArray(temp), cipherInt, General.NewLine));
                int plainInt = (int)Decrypt(cipherInt, d, p);
                sb.Append(String.Format("Decrypt({0}) = {1} ^ {2} mod {3} = {4}{5}", cipherInt, cipherInt, d, p, plainInt, General.NewLine));
                plainText += (char)plainInt;

                byte[] plain = BitConverter.GetBytes(plainInt);
                for (int j = 0; j < plain.Length; j++)
                {
                    plainByte[i * 4 + j] = plain[j];
                }
                if(base64)
                    sb.Append(String.Format("{0} to byte array = {1}{2}", plainInt, General.printByteArray(plain), General.NewLine));
                else
                    sb.Append(String.Format("{0} = {1} {2}", plainInt, (char)plainInt, General.NewLine));

                sb.Append(General.NewLine);
            }

            if (base64)
                plainText = Convert.ToBase64String(plainByte);

            sb.Append(String.Format("Decrypt text = {0}{1}", plainText, General.NewLine));
            return plainText;
        }
    }
}
