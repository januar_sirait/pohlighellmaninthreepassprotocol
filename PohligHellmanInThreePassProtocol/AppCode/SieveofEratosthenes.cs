﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PohligHellmanInThreePassProtocol.AppCode
{
    class SieveofEratosthenes
    {
        public static List<int> Generate(int min, int limit)
        {
            bool[] bil_asal = new bool[limit];
            for (int i = 0; i < limit; i++)
            {
                bil_asal[i] = true;
            }

            bil_asal[0] = false; // bilangan 0 bukan bilangan prima
            bil_asal[1] = false; // bilangan 1 bukan bilangan prima

            // Penerapan algoritma Sieve of Erathosthenes
            for (int i = 2; i <= Math.Sqrt(limit); i++)
            {
                if (bil_asal[i])
                {
                    for (int j = i * i; j < limit; j = j + i)
                    {
                        bil_asal[j] = false;
                    }
                }
            }

            List<int> prima = new List<int>();
            for (int i = 2; i < limit; i++)
            {
                if (bil_asal[i] && i >= min)
                {
                    prima.Add(i);
                }
            }

            Console.WriteLine("Jumlah bilangan prima antara " + min + " sampai " + limit
                + " adalah " + prima.Count + " buah");

            return prima;
        }

        public static List<int> Generate()
        {
            return SieveofEratosthenes.Generate(2, 99999999);
        }

        public static List<int> Generate(int limit)
        {
            return SieveofEratosthenes.Generate(2, limit);
        }

        public static List<int> GenerateMin(int min)
        {
            return SieveofEratosthenes.Generate(min, 99999999);
        }
    }
}
