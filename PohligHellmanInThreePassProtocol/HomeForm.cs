﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PohligHellmanInThreePassProtocol.AppCode;

namespace PohligHellmanInThreePassProtocol
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
        }

        private void PaintBorderlessGroupBox(object sender, PaintEventArgs p)
        {
            GroupBox box = (GroupBox)sender;
            DrawGroupBox(box, p.Graphics, Color.Black, Color.DarkGray);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Clear text and border
                g.Clear(this.BackColor);

                // Draw text
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void btnGenerateKey_Click(object sender, EventArgs e)
        {
            PohligHellman pohlig = new PohligHellman();
            int p = pohlig.GetP();
            pohlig.sb.Append(General.NewLine + "GENERATE KEY FOR SENDER" + General.NewLine);
            pohlig.sb.Append(String.Format("p = {0}{1}", p, General.NewLine));
            pohlig.sb.Append(String.Format("totien p = {0}{1}", p-1, General.NewLine));

            pohlig.sb.Append(String.Format("{0}Generate e{1}", General.NewLine, General.NewLine));
            int eSender = pohlig.GetE(p - 1);
            pohlig.sb.Append(String.Format("{0}Generate d using Extended Euclidean{1}", General.NewLine, General.NewLine));
            int dSender = pohlig.ExtendedEuclidean(p - 1, eSender);
            pohlig.sb.Append(General.NewLine + General.DrawLine() + General.NewLine);

            txtPSender.Text = p.ToString();
            txtESender.Text = eSender.ToString();
            txtDSender.Text = dSender.ToString();

            pohlig.sb.Append(General.NewLine + "GENERATE KEY FOR RECEIVER" + General.NewLine);
            pohlig.sb.Append(String.Format("p = {0}{1}", p, General.NewLine));
            pohlig.sb.Append(String.Format("totien p = {0}{1}", p - 1, General.NewLine));

            pohlig.sb.Append(String.Format("{0}Generate e{1}", General.NewLine, General.NewLine));
            int eReceiver = pohlig.GetE(p - 1);

            pohlig.sb.Append(String.Format("{0}Generate d using Extended Euclidean{1}", General.NewLine, General.NewLine));
            int dReceiver = pohlig.ExtendedEuclidean(p - 1, eReceiver);

            txtPReceiver.Text = p.ToString();
            txtEReceiver.Text = eReceiver.ToString();
            txtDReceiver.Text = dReceiver.ToString();
            txtLogKey.Text = pohlig.sb.ToString();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (!General.isNumber(txtPSender.Text) &&
                !General.isNumber(txtESender.Text) &&
                !General.isNumber(txtDSender.Text) &&
                !General.isNumber(txtEReceiver.Text) &&
                !General.isNumber(txtDReceiver.Text))
            {
                MessageBox.Show("Please generate valide key!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (txtMessage.Text == "")
            {
                MessageBox.Show("Please insert message!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            int p = Int32.Parse(txtPSender.Text);
            int eSender = Int32.Parse(txtESender.Text);
            int dSender = Int32.Parse(txtDSender.Text);
            int eReceiver = Int32.Parse(txtEReceiver.Text);
            int dReceiver = Int32.Parse(txtDReceiver.Text);

            //int m = Int32.Parse(txtMessage.Text);

            PohligHellman pohlig = new PohligHellman();
            System.Diagnostics.Stopwatch time = new System.Diagnostics.Stopwatch();
            System.Diagnostics.Stopwatch timeCurrent = new System.Diagnostics.Stopwatch();
            time.Start();
            // 1st
            timeCurrent.Start();
            pohlig.sb.Append("SENDER" + General.NewLine);
            pohlig.sb.Append("Message = " + txtMessage.Text + General.NewLine);
            pohlig.sb.Append("Sender encrypt message ..." + General.NewLine); 
            string senderEnc = pohlig.Encrypt(txtMessage.Text, eSender, p);
            txtEncSender.Text = senderEnc;
            txtRecReceiver1.Text = senderEnc;
            pohlig.sb.Append("----------End----------" + General.NewLine + General.NewLine);
            timeCurrent.Stop();
            lblRun1.Text = timeCurrent.ElapsedMilliseconds.ToString() + " ms";
            timeCurrent.Reset();

            // 2nd
            timeCurrent.Start();
            pohlig.sb.Append("RECEIVER" + General.NewLine);
            pohlig.sb.Append("Message = " + senderEnc + General.NewLine);
            pohlig.sb.Append("Receiver encrypt message ..." + General.NewLine);
            string receiverEnc = pohlig.Encrypt(senderEnc, eReceiver, p, true);
            txtEncReceiver.Text = receiverEnc;
            txtRecSender.Text = receiverEnc;
            pohlig.sb.Append("----------End----------" + General.NewLine + General.NewLine);
            timeCurrent.Stop();
            lblRun2.Text = timeCurrent.ElapsedMilliseconds.ToString() + " ms";
            timeCurrent.Reset();

            // 3th
            timeCurrent.Start();
            pohlig.sb.Append("SENDER" + General.NewLine);
            pohlig.sb.Append("Message = " + receiverEnc + General.NewLine);
            pohlig.sb.Append("Sender decrypt message ..." + General.NewLine); 
            string senderDec = pohlig.Decrypt(receiverEnc, dSender, p, true);
            txtDecSender.Text = senderDec;
            txtRecReceiver2.Text = senderDec;
            pohlig.sb.Append("----------End----------" + General.NewLine + General.NewLine);
            timeCurrent.Stop();
            lblRun3.Text = timeCurrent.ElapsedMilliseconds.ToString() + " ms";
            timeCurrent.Reset();

            timeCurrent.Start();
            pohlig.sb.Append("RECEIVER" + General.NewLine);
            pohlig.sb.Append("Message = " + senderDec + General.NewLine);
            pohlig.sb.Append("Receiver decrypt message ..." + General.NewLine);
            txtDecReceiver.Text = pohlig.Decrypt(senderDec, dReceiver, p);
            timeCurrent.Stop();
            lblRun4.Text = timeCurrent.ElapsedMilliseconds.ToString() + " ms";
            time.Stop();

            txtLog.Text = pohlig.sb.ToString();
            lblRunningTime.Text = time.ElapsedMilliseconds.ToString() + " ms";
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm about = new AboutForm();
            about.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void encryptFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EncryptFileForm encFile = new EncryptFileForm();
            encFile.ShowDialog();
        }
    }
}
