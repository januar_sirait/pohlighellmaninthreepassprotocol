﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

using PohligHellmanInThreePassProtocol.AppCode;

namespace PohligHellmanInThreePassProtocol
{
    public partial class EncryptFileForm : Form
    {
        bool procesedFinished = false;
        int percentage = 0;
        bool change = false;
        string path;
        delegate void SetTextCallback(string text, int type);

        public EncryptFileForm()
        {
            InitializeComponent();
        }

        private void SetText(string text, int type)
        {
            toolStripLabel.Text = text;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //Your background task goes here
            while (procesedFinished == false)
            {
                if (change)
                {
                    backgroundWorker1.ReportProgress(percentage);
                    change = false;
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void btnGenerateKey_Click(object sender, EventArgs e)
        {
            PohligHellman pohlig = new PohligHellman();
            int p = pohlig.GetP();
            pohlig.sb.Append(General.NewLine + "GENERATE KEY FOR SENDER" + General.NewLine);
            pohlig.sb.Append(String.Format("p = {0}{1}", p, General.NewLine));
            pohlig.sb.Append(String.Format("totien p = {0}{1}", p - 1, General.NewLine));

            pohlig.sb.Append(String.Format("{0}Generate e{1}", General.NewLine, General.NewLine));
            int eSender = pohlig.GetE(p - 1);
            pohlig.sb.Append(String.Format("{0}Generate d using Extended Euclidean{1}", General.NewLine, General.NewLine));
            int dSender = pohlig.ExtendedEuclidean(p - 1, eSender);
            pohlig.sb.Append(General.NewLine + General.DrawLine() + General.NewLine);

            txtPSender.Text = p.ToString();
            txtESender.Text = eSender.ToString();
            txtDSender.Text = dSender.ToString();

            pohlig.sb.Append(General.NewLine + "GENERATE KEY FOR RECEIVER" + General.NewLine);
            pohlig.sb.Append(String.Format("p = {0}{1}", p, General.NewLine));
            pohlig.sb.Append(String.Format("totien p = {0}{1}", p - 1, General.NewLine));

            pohlig.sb.Append(String.Format("{0}Generate e{1}", General.NewLine, General.NewLine));
            int eReceiver = pohlig.GetE(p - 1);

            pohlig.sb.Append(String.Format("{0}Generate d using Extended Euclidean{1}", General.NewLine, General.NewLine));
            int dReceiver = pohlig.ExtendedEuclidean(p - 1, eReceiver);

            txtPReceiver.Text = p.ToString();
            txtEReceiver.Text = eReceiver.ToString();
            txtDReceiver.Text = dReceiver.ToString();
            txtLogKey.Text = pohlig.sb.ToString();
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (!General.isNumber(txtPSender.Text) &&
                !General.isNumber(txtESender.Text) &&
                !General.isNumber(txtDSender.Text) &&
                !General.isNumber(txtEReceiver.Text) &&
                !General.isNumber(txtDReceiver.Text))
            {
                MessageBox.Show("Please generate valide key!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (openFileDialog1.FileNames.Count() == 0)
            {
                MessageBox.Show("Please select file to encrypt!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int p = Int32.Parse(txtPSender.Text);
            int eSender = Int32.Parse(txtESender.Text);
            int dSender = Int32.Parse(txtDSender.Text);
            int eReceiver = Int32.Parse(txtEReceiver.Text);
            int dReceiver = Int32.Parse(txtDReceiver.Text);

            //int m = Int32.Parse(txtMessage.Text);

            SetTextCallback de = new SetTextCallback(SetText);
            progressBar1.Visible = true;
            procesedFinished = false;
            percentage = 0;
            progressBar1.Value = 0;

            System.Diagnostics.Stopwatch time = new System.Diagnostics.Stopwatch();
            String message = ReadFile(openFileDialog1.FileName);
            String fileExtention = Path.GetExtension(openFileDialog1.FileName);
            String fileName = Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
            path = Path.GetDirectoryName(openFileDialog1.FileName) + "\\";
            
            backgroundWorker1.RunWorkerAsync();
            Thread thread = new Thread(new ThreadStart(() => 
            {
                this.Invoke(de, new object[] { "Processing ...", 0 });
                time.Start();

                PohligHellman pohlig = new PohligHellman();
                // 1st
                pohlig.sb.Append("SENDER" + General.NewLine);
                pohlig.sb.Append("Message = " + message + General.NewLine);
                pohlig.sb.Append("Sender encrypt message ..." + General.NewLine);
                string senderEnc = pohlig.Encrypt(message, eSender, p);
                WriteToFile(senderEnc, path + fileName + "_SenderEncrypt" + fileExtention);
                pohlig.sb.Append("----------End----------" + General.NewLine + General.NewLine);
                percentage = 3;
                change = true;

                // 2nd
                pohlig.sb.Append("RECEIVER" + General.NewLine);
                pohlig.sb.Append("Message = " + senderEnc + General.NewLine);
                pohlig.sb.Append("Receiver encrypt message ..." + General.NewLine);
                string receiverEnc = pohlig.Encrypt(senderEnc, eReceiver, p, true);
                WriteToFile(receiverEnc, path + fileName + "_ReceiverEncrypt" + fileExtention);
                pohlig.sb.Append("----------End----------" + General.NewLine + General.NewLine);
                percentage = 6;
                change = true;

                // 3th
                pohlig.sb.Append("SENDER" + General.NewLine);
                pohlig.sb.Append("Message = " + receiverEnc + General.NewLine);
                pohlig.sb.Append("Sender decrypt message ..." + General.NewLine);
                string senderDec = pohlig.Decrypt(receiverEnc, dSender, p, true);
                WriteToFile(senderDec, path + fileName + "_SenderDencrypt" + fileExtention);
                pohlig.sb.Append("----------End----------" + General.NewLine + General.NewLine);
                percentage = 9;
                change = true;

                pohlig.sb.Append("RECEIVER" + General.NewLine);
                pohlig.sb.Append("Message = " + senderDec + General.NewLine);
                pohlig.sb.Append("Receiver decrypt message ..." + General.NewLine);
                string receiverDec = pohlig.Decrypt(senderDec, dReceiver, p);
                WriteToFile(receiverDec, path + fileName + "_ReceiverDencrypt" + fileExtention);
                percentage = 10;
                change = true;

                time.Stop();
                procesedFinished = true;

                this.Invoke(de, new object[] { "Finish : " + time.ElapsedMilliseconds.ToString() + " ms", 0 });
            }));

            thread.Start();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            txtFile.Text = openFileDialog1.FileName;
        }

        private void WriteToFile(String text, String filename)
        {
            using (StreamWriter sw = new StreamWriter(filename)) {
                sw.Write(text);
            }
        }

        private String ReadFile(String filename)
        {
            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = new StreamReader(filename))
            {
                sb.Append(sr.ReadToEnd());
            }

            return sb.ToString();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DialogResult result = MessageBox.Show("Success!! Do you want to open folder of encrypted file?", "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                System.Diagnostics.Process.Start(path);
            }

            progressBar1.Value = 0;
            progressBar1.Visible = false;
        }
    }
}
