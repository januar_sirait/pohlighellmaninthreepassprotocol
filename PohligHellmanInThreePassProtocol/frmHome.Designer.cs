﻿namespace PohligHellmanInThreePassProtocol
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtLogKey = new System.Windows.Forms.TextBox();
            this.btnGenerateKey = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtDReceiver = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEReceiver = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPReceiver = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDSender = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtESender = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPSender = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtDecReceiver = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtRecReceiver2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtEncReceiver = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRecReceiver1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblRunningTime = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDecSender = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtRecSender = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEncSender = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.encryptFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblRun1 = new System.Windows.Forms.Label();
            this.lblRun2 = new System.Windows.Forms.Label();
            this.lblRun4 = new System.Windows.Forms.Label();
            this.lblRun3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtLogKey);
            this.groupBox1.Controls.Add(this.btnGenerateKey);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(912, 155);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "User Key";
            this.groupBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintBorderlessGroupBox);
            // 
            // txtLogKey
            // 
            this.txtLogKey.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLogKey.Location = new System.Drawing.Point(459, 14);
            this.txtLogKey.Multiline = true;
            this.txtLogKey.Name = "txtLogKey";
            this.txtLogKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLogKey.Size = new System.Drawing.Size(447, 104);
            this.txtLogKey.TabIndex = 8;
            // 
            // btnGenerateKey
            // 
            this.btnGenerateKey.Location = new System.Drawing.Point(810, 124);
            this.btnGenerateKey.Name = "btnGenerateKey";
            this.btnGenerateKey.Size = new System.Drawing.Size(96, 23);
            this.btnGenerateKey.TabIndex = 7;
            this.btnGenerateKey.Text = "Generate Key";
            this.btnGenerateKey.UseVisualStyleBackColor = true;
            this.btnGenerateKey.Click += new System.EventHandler(this.btnGenerateKey_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtDReceiver);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtEReceiver);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtPReceiver);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(233, 14);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(206, 104);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Receiver Key";
            this.groupBox3.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintBorderlessGroupBox);
            // 
            // txtDReceiver
            // 
            this.txtDReceiver.BackColor = System.Drawing.SystemColors.Window;
            this.txtDReceiver.Location = new System.Drawing.Point(65, 72);
            this.txtDReceiver.Name = "txtDReceiver";
            this.txtDReceiver.ReadOnly = true;
            this.txtDReceiver.Size = new System.Drawing.Size(100, 20);
            this.txtDReceiver.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "d";
            // 
            // txtEReceiver
            // 
            this.txtEReceiver.BackColor = System.Drawing.SystemColors.Window;
            this.txtEReceiver.Location = new System.Drawing.Point(65, 46);
            this.txtEReceiver.Name = "txtEReceiver";
            this.txtEReceiver.ReadOnly = true;
            this.txtEReceiver.Size = new System.Drawing.Size(100, 20);
            this.txtEReceiver.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "e";
            // 
            // txtPReceiver
            // 
            this.txtPReceiver.BackColor = System.Drawing.SystemColors.Window;
            this.txtPReceiver.Location = new System.Drawing.Point(65, 20);
            this.txtPReceiver.Name = "txtPReceiver";
            this.txtPReceiver.ReadOnly = true;
            this.txtPReceiver.Size = new System.Drawing.Size(100, 20);
            this.txtPReceiver.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "p";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtDSender);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtESender);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtPSender);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(15, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(206, 104);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sender Key";
            this.groupBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintBorderlessGroupBox);
            // 
            // txtDSender
            // 
            this.txtDSender.BackColor = System.Drawing.SystemColors.Window;
            this.txtDSender.Location = new System.Drawing.Point(65, 72);
            this.txtDSender.Name = "txtDSender";
            this.txtDSender.ReadOnly = true;
            this.txtDSender.Size = new System.Drawing.Size(100, 20);
            this.txtDSender.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "d";
            // 
            // txtESender
            // 
            this.txtESender.BackColor = System.Drawing.SystemColors.Window;
            this.txtESender.Location = new System.Drawing.Point(65, 46);
            this.txtESender.Name = "txtESender";
            this.txtESender.ReadOnly = true;
            this.txtESender.Size = new System.Drawing.Size(100, 20);
            this.txtESender.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "e";
            // 
            // txtPSender
            // 
            this.txtPSender.BackColor = System.Drawing.SystemColors.Window;
            this.txtPSender.Location = new System.Drawing.Point(65, 20);
            this.txtPSender.Name = "txtPSender";
            this.txtPSender.ReadOnly = true;
            this.txtPSender.Size = new System.Drawing.Size(100, 20);
            this.txtPSender.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "p";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtLog);
            this.groupBox4.Controls.Add(this.btnProcess);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.txtMessage);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Location = new System.Drawing.Point(12, 182);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(912, 497);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Three Pass Protocol";
            this.groupBox4.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintBorderlessGroupBox);
            // 
            // txtLog
            // 
            this.txtLog.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLog.Location = new System.Drawing.Point(15, 339);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(886, 156);
            this.txtLog.TabIndex = 8;
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(810, 21);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(96, 23);
            this.btnProcess.TabIndex = 7;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblRun4);
            this.groupBox6.Controls.Add(this.lblRun2);
            this.groupBox6.Controls.Add(this.txtDecReceiver);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.txtRecReceiver2);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.txtEncReceiver);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.txtRecReceiver1);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Location = new System.Drawing.Point(477, 51);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(424, 284);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Receiver";
            this.groupBox6.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintBorderlessGroupBox);
            // 
            // txtDecReceiver
            // 
            this.txtDecReceiver.Location = new System.Drawing.Point(114, 212);
            this.txtDecReceiver.Multiline = true;
            this.txtDecReceiver.Name = "txtDecReceiver";
            this.txtDecReceiver.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDecReceiver.Size = new System.Drawing.Size(304, 51);
            this.txtDecReceiver.TabIndex = 7;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 215);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(108, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Dencrypted Message";
            // 
            // txtRecReceiver2
            // 
            this.txtRecReceiver2.Location = new System.Drawing.Point(114, 155);
            this.txtRecReceiver2.Multiline = true;
            this.txtRecReceiver2.Name = "txtRecReceiver2";
            this.txtRecReceiver2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRecReceiver2.Size = new System.Drawing.Size(304, 51);
            this.txtRecReceiver2.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 158);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Received Message";
            // 
            // txtEncReceiver
            // 
            this.txtEncReceiver.Location = new System.Drawing.Point(114, 88);
            this.txtEncReceiver.Multiline = true;
            this.txtEncReceiver.Name = "txtEncReceiver";
            this.txtEncReceiver.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtEncReceiver.Size = new System.Drawing.Size(304, 51);
            this.txtEncReceiver.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 91);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Encrypted Message";
            // 
            // txtRecReceiver1
            // 
            this.txtRecReceiver1.Location = new System.Drawing.Point(114, 22);
            this.txtRecReceiver1.Multiline = true;
            this.txtRecReceiver1.Name = "txtRecReceiver1";
            this.txtRecReceiver1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRecReceiver1.Size = new System.Drawing.Size(304, 51);
            this.txtRecReceiver1.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(99, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Received Message";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblRun3);
            this.groupBox5.Controls.Add(this.lblRun1);
            this.groupBox5.Controls.Add(this.lblRunningTime);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtDecSender);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.txtRecSender);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.txtEncSender);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Location = new System.Drawing.Point(15, 49);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(424, 284);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Sender";
            this.groupBox5.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintBorderlessGroupBox);
            // 
            // lblRunningTime
            // 
            this.lblRunningTime.AutoSize = true;
            this.lblRunningTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRunningTime.Location = new System.Drawing.Point(111, 243);
            this.lblRunningTime.Name = "lblRunningTime";
            this.lblRunningTime.Size = new System.Drawing.Size(107, 15);
            this.lblRunningTime.TabIndex = 7;
            this.lblRunningTime.Text = "                         ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 243);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 15);
            this.label15.TabIndex = 6;
            this.label15.Text = "Running time : ";
            // 
            // txtDecSender
            // 
            this.txtDecSender.Location = new System.Drawing.Point(114, 155);
            this.txtDecSender.Multiline = true;
            this.txtDecSender.Name = "txtDecSender";
            this.txtDecSender.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDecSender.Size = new System.Drawing.Size(304, 51);
            this.txtDecSender.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 158);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Dencrypted Message";
            // 
            // txtRecSender
            // 
            this.txtRecSender.Location = new System.Drawing.Point(114, 88);
            this.txtRecSender.Multiline = true;
            this.txtRecSender.Name = "txtRecSender";
            this.txtRecSender.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRecSender.Size = new System.Drawing.Size(304, 51);
            this.txtRecSender.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Received Message";
            // 
            // txtEncSender
            // 
            this.txtEncSender.Location = new System.Drawing.Point(114, 22);
            this.txtEncSender.Multiline = true;
            this.txtEncSender.Name = "txtEncSender";
            this.txtEncSender.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtEncSender.Size = new System.Drawing.Size(304, 51);
            this.txtEncSender.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Encrypted Message";
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(80, 23);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(714, 20);
            this.txtMessage.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Message";
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.ShowItemToolTips = true;
            this.menuStrip1.Size = new System.Drawing.Size(936, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.encryptFileToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // encryptFileToolStripMenuItem
            // 
            this.encryptFileToolStripMenuItem.Name = "encryptFileToolStripMenuItem";
            this.encryptFileToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.encryptFileToolStripMenuItem.Text = "Encrypt File";
            this.encryptFileToolStripMenuItem.Click += new System.EventHandler(this.encryptFileToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // lblRun1
            // 
            this.lblRun1.AutoSize = true;
            this.lblRun1.Location = new System.Drawing.Point(7, 60);
            this.lblRun1.Name = "lblRun1";
            this.lblRun1.Size = new System.Drawing.Size(31, 13);
            this.lblRun1.TabIndex = 8;
            this.lblRun1.Text = "        ";
            // 
            // lblRun2
            // 
            this.lblRun2.AutoSize = true;
            this.lblRun2.Location = new System.Drawing.Point(7, 126);
            this.lblRun2.Name = "lblRun2";
            this.lblRun2.Size = new System.Drawing.Size(31, 13);
            this.lblRun2.TabIndex = 9;
            this.lblRun2.Text = "        ";
            // 
            // lblRun4
            // 
            this.lblRun4.AutoSize = true;
            this.lblRun4.Location = new System.Drawing.Point(7, 250);
            this.lblRun4.Name = "lblRun4";
            this.lblRun4.Size = new System.Drawing.Size(31, 13);
            this.lblRun4.TabIndex = 10;
            this.lblRun4.Text = "        ";
            // 
            // lblRun3
            // 
            this.lblRun3.AutoSize = true;
            this.lblRun3.Location = new System.Drawing.Point(7, 193);
            this.lblRun3.Name = "lblRun3";
            this.lblRun3.Size = new System.Drawing.Size(31, 13);
            this.lblRun3.TabIndex = 11;
            this.lblRun3.Text = "        ";
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 682);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "HomeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Implementation Three Pass Protocol using Pohlig-Helman Algorithm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDSender;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtESender;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPSender;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtDReceiver;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEReceiver;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPReceiver;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGenerateKey;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEncSender;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtRecSender;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDecSender;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtRecReceiver2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtEncReceiver;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRecReceiver1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDecReceiver;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.TextBox txtLogKey;
        private System.Windows.Forms.Label lblRunningTime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem encryptFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label lblRun4;
        private System.Windows.Forms.Label lblRun2;
        private System.Windows.Forms.Label lblRun3;
        private System.Windows.Forms.Label lblRun1;

    }
}

